# alpine-dnsmasq

dnsmasq requires NET_ADMIN capabilities to run correctly. Start it with something like docker run -p 53:53/tcp -p 53:53/udp --cap-add=NET_ADMIN


    version: '3'
    services:
        dnsmasq:
            image: registry.gitlab.com/borian/alpine-dnsmasq
            ports:
                - "53:53/tcp"
                - "53:53/udp"
            cap_add:
                - NET_ADMIN
            command: --log-facility=-
            volumes:
                - ./dnsmasq.conf:/etc/dnsmasq.conf