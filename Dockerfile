FROM alpine:latest
RUN apk --update --no-cache add dnsmasq
EXPOSE 53 53/udp
CMD ["dnsmasq", "-k"]